$(function () {

	let language_code = $('#language-code').text();
	let language_url = $('#language-url').text();
	let domain = $('#domain').text();

	// GET INFO
	(function(){
		$.ajax({
			method: 'POST',
			url: domain + "" + language_url + "ajax/info/",
		}).done(function(data) {
			$('span#favourite-total').text(data['favourites_total']);
			$('span#last_played-total').text(data['last_played_total']);
		});
		return false;
	})();

	$('#request-fullscreen').click(function () {
		// screenfull.request();
		screenfull.request($('#game-code')[0])
	});

	// Set language
	(function(){
		$('.set-language-link').on('click', function(e) {
			let lang = $(this).data('lang');
			$('#set-language-form select[name="language"] option:selected').prop("selected", false);
			$('#set-language-form select[name="language"] option[value="' + lang + '"]').prop("selected",true);
			$('#set-language-form').submit();
			return false;
		});
	})();

	// Doesnt work button
	(function(){
		$('#doesnt-work').click(function () {
			let id = $(this).data('id');
			$.ajax({
				method: 'POST',
				url: domain + "" + language_url + "ajax/doesnt-work/" + id + "/",
			}).done(function() {
				alert('Ok');
			});
			return false;
		})
	})();


	// Like/Dislike
	(function(){
		$('.like-ajax').click(function () {
			let id = $(this).data('id');
			let action = $(this).data('action');
			let block = $(this).find('span');
			$.ajax({
				method: 'POST',
				url: domain + "" + language_url + "ajax/like/" + id + "/" + action + '/',
			}).done(function(data) {
				block.text(data['total']);
			});
			return false;
		})
	})();

	// Add to favourites
	(function(){
		$('.add-to-favourites').click(function () {
			let id = $(this).data('id');
			let block_total = $('#favourite-total');
			let button = $(this).find('span');
			$.ajax({
				method: 'POST',
				url: domain + "" + language_url + "ajax/add-to-favourites/" + id + "/",
			}).done(function(data) {
				block_total.text(data['total']);
				button.text(data['text']);
				$(this).css('cursor', 'default !important');
			});
			return false;
		})
	})();

	// Clear session
	(function(){
		$(document).on('click', '#clear-session', function () {
			let action = $(this).data('action');
			let favourite_total = $('#favourite-total');
			let last_played_total = $('#last_played-total');
			$.ajax({
				method: 'POST',
				url: domain + "" + language_url + "ajax/clear-session/" + action + '/',
			}).done(function(data) {
				if (data['action'] == 'favourites') {
					favourite_total.text(0);
				} else {
					last_played_total.text(0);
				}
				$('.close-modal').click();
			});
			return false;
		})
	})();

	// Embed code
	(function(){
		$('#show-embed').click(function () {
			let id = $(this).data('id');
			$.ajax({
				method: 'POST',
				url: domain + "" + language_url + "ajax/embed-code/" + id + "/",
			}).done(function(data) {
				$('.game-frame').html(data['code']);
				$('.game-preloader').hide();
				$('.game-frame').show();
			});
			return false;
		});
	})();
});

//Django basic setup for accepting ajax requests.
function getCookie(name) {
	var cookieValue = null;
	if (document.cookie && document.cookie != '') {
		var cookies = document.cookie.split(';');
		for (var i = 0; i < cookies.length; i++) {
			var cookie = jQuery.trim(cookies[i]);
			// Does this cookie string begin with the name we want?
			if (cookie.substring(0, name.length + 1) == (name + '=')) {
				cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
				break;
			}
		}
	}
	return cookieValue;
}
var csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
	// these HTTP methods do not require CSRF protection
	return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
function sameOrigin(url) {
	// test that a given url is a same-origin URL
	// url could be relative or scheme relative or absolute
	var host = document.location.host; // host + port
	var protocol = document.location.protocol;
	var sr_origin = '//' + host;
	var origin = protocol + sr_origin;
	// Allow absolute or scheme relative URLs to same origin
	return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
		(url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
		// or any other URL that isn't scheme relative or absolute i.e relative.
		!(/^(\/\/|http:|https:).*/.test(url));
}
$.ajaxSetup({
	beforeSend: function(xhr, settings) {
		if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
			// Send the token to same-origin, relative URLs only.
			// Send the token only if the method warrants CSRF protection
			// Using the CSRFToken value acquired earlier
			xhr.setRequestHeader("X-CSRFToken", csrftoken);
		}
	}
});