# -*- coding: utf-8 -*-
import re

from django.core.management.base import BaseCommand
from tqdm import tqdm

from catalog.models import GameParse

site = 'http://www.agame.com'


class Command(BaseCommand):

    def handle(self, *args, **options):
        for o in tqdm(GameParse.objects.all().exclude(title='')):

            code = o.code.replace(' ', '').replace('\\n', '')
            m = re.search("settings:{src:(.+?)gp=1", code)
            if m:
                o.code_clear = m.group(1).replace("\\'//", '//').replace("\\'", '')[:-1]
                o.save()
