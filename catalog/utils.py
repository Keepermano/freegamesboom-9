# -*- coding: utf-8 -*-
import hashlib
import time

from django.contrib.contenttypes.models import ContentType
from django.http import HttpResponseBadRequest


def upload_dir(instance, filename):
    tmp = '%s:%s' % (instance, str(time.time()))
    m = hashlib.md5(tmp.encode('utf-8')).hexdigest()
    ctype = ContentType.objects.get_for_model(instance)
    model = ctype.model
    return 'upload/%s/%s/%s.%s' % (model, m[:2], m, filename.split('.')[-1])


def mobile_order(qs, request, order='-play_counter'):
    # if request.user_agent.is_mobile or request.user_agent.is_tablet:
    #     return qs.exclude(mobile=False).order_by('-play_counter_mobile' if order == '-play_counter' else order)
    return qs.order_by(order)


def require_ajax(func):
    def decorator(request, *args, **kwargs):
        if not request.is_ajax():
            return HttpResponseBadRequest()
        return func(request, *args, **kwargs)
    return decorator
