from django.urls import path
from django.contrib.sitemaps import GenericSitemap, Sitemap, views
from django.contrib.sitemaps.views import sitemap

from .views import *
from .models import *


class GameSitemap(Sitemap):
    changefreq = "always"
    priority = 0.5
    limit = 5000

    def items(self):
        return Game.objects.filter(insecure=False)

    def lastmod(self, item):
        return item.modified


sitemaps = {
    'games': GameSitemap(),
    'categories': GenericSitemap({
        'queryset': Category.published.all(),
        'date_field': 'modified'
    }, priority=0.5, changefreq='always'),
    'tags': GenericSitemap({
        'queryset': Tag.published.all(),
        'date_field': 'modified'
    }, priority=0.5, changefreq='always'),
}

cache_time = 60 * 60 * 2

urlpatterns = [
    path('', cache_page(cache_time)(TemplateView.as_view(template_name="catalog/index.html")), name='index'),
    path('show/<action>/', cache_page(cache_time)(game_list), name='game-list'),
    path('search/', cache_page(cache_time)(SearchView.as_view()), name='search'),
    path('tags/', cache_page(cache_time)(TagList.as_view()), name='tags'),
    path('<slug>/', cache_page(cache_time)(game_list_by_category), name='category-detail'),
    path('games/tag/<slug>/', cache_page(cache_time)(game_list_by_tag), name='tag-detail'),
    path('online/games/<slug>/full/', cache_page(cache_time)(GameDetailNoSecureFull.as_view()), name='game-detail-insecure-full'),
    path('online/games/<slug>/', cache_page(cache_time)(GameDetailNoSecure.as_view()), name='game-detail-insecure'),
    path('games/<slug>/full/', cache_page(cache_time)(GameDetailFull.as_view()), name='game-detail-full'),
    path('games/<slug>/', cache_page(cache_time)(GameDetail.as_view()), name='game-detail'),
    path('sitemap.xml', cache_page(cache_time)(sitemap), {'sitemaps': sitemaps}, name='django.contrib.sitemaps.views.sitemap'),
    path('sitemap-<section>.xml', cache_page(cache_time)(views.sitemap), {'sitemaps': sitemaps},
         name='django.contrib.sitemaps.views.sitemap'),

    path('ajax/doesnt-work/<int:pk>/', DoestWorkAjax.as_view(), name='doesnt-work-ajax'),
    path('ajax/like/<int:pk>/<action>/', LikeDislikeAjax.as_view(), name='like-ajax'),
    path('ajax/embed-code/<int:pk>/', cache_page(cache_time)(EmbedCodeAjax.as_view()), name='embed-code-ajax'),
    path('ajax/popup-games/<action>/', PopupGamesAjax.as_view(), name='popup-games-ajax'),
    path('ajax/add-to-favourites/<int:pk>/', AddToFavouritesAjax.as_view(), name='add-to-favourites-ajax'),
    path('ajax/clear-session/<action>/', ClearSessionAction.as_view(), name='clear-session'),
    path('ajax/autocomplete/', cache_page(cache_time)(autocomplete_search), name='autocomplete-search'),
    path('ajax/info/', AjaxInfo.as_view(), name='ajax-info'),
]
